using System;
using System.Collections.Generic;
using System.Linq;
using LeoECSLiteExtensions.Common;
using LeoECSLiteExtensions.OneFrame.Attributes;
using LeoECSLiteExtensions.OneFrame.Intefaces;
using LeoECSLiteExtensions.OneFrame.Systems;
using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedSystems;
using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions.OneFrame
{
    public class EcsOneFrameInstaller : Installer<EcsOneFrameInstaller>
    {
        [Preserve]
        public EcsOneFrameInstaller()
        {
        }

        public override void InstallBindings()
        {
            Container.BindFactory<EcsGroupSystem, Factory>()
                .FromSubContainerResolve()
                .ByMethod(container =>
                {
                    var ecsOneFrameSystemOpen = typeof(EcsOneFrameSystem<>.Factory);

                    var ecsOneFrameSystemParameter = ecsOneFrameSystemOpen.GetGenericArguments()[0]; //because one
                    var constraints = ecsOneFrameSystemParameter.GetGenericParameterConstraints();

                    var allTypes = TypeExtensions
                        .FindTypesWithAttribute<EcsOneFrameAttribute>()
                        .SelectMany(type =>
                        {
                            if (type.IsGenericTypeDefinition) return type.GetAllPossibleClosedTypes();

                            return LinqExtensions.FromParams(type);
                        })
                        .Where(type => constraints.Length == 0
                                       || constraints.All(constraint => constraint.IsAssignableFrom(type)));

                    foreach (var type in allTypes)
                    {
                        var closeType = ecsOneFrameSystemOpen.MakeGenericType(type);

                        container
                            .Bind<IOneFrameSystemFactory>()
                            .To(closeType)
                            .AsSingle();
                    }

                    var creator = container.Instantiate<GroupCreator>();
                    var instance = creator.CreateGroup();

                    container
                        .Bind<EcsGroupSystem>()
                        .FromInstance(instance)
                        .AsSingle();
                })
                .AsSingle();
        }

        public class Factory : PlaceholderFactory<EcsGroupSystem>
        {
            [Preserve]
            public Factory()
            {
            }
        }

        private class GroupCreator
        {
            private readonly List<IOneFrameSystemFactory> _factories;

            [Preserve]
            public GroupCreator(List<IOneFrameSystemFactory> factories)
            {
                _factories = factories ?? throw new ArgumentNullException(nameof(factories));
            }

            public EcsGroupSystem CreateGroup()
            {
                var statsSystems = _factories.Count > 0
                    ? _factories
                        .Select(factory => factory.Create())
                        .ToArray()
                    : new IEcsSystem[] {new FakeSystem()};

                var group = new EcsGroupSystem(nameof(EcsOneFrameInstaller), true, null, statsSystems);

                return group;
            }

            private class FakeSystem : IEcsSystem
            {
            }
        }
    }
}