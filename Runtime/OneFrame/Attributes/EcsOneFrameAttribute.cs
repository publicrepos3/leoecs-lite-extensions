using System;

namespace LeoECSLiteExtensions.OneFrame.Attributes
{
    [AttributeUsage(AttributeTargets.Struct)]
    public class EcsOneFrameAttribute : Attribute
    {
    }
}