using System;
using LeoECSLiteExtensions.OneFrame.Intefaces;
using LeoECSLiteExtensions.ZenjectBridge;
using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using Leopotam.EcsLite;
using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions.OneFrame.Systems
{
    public class EcsOneFrameSystem<T> : IEcsRunSystem
        where T : struct
    {
        private readonly IEcsFilter<ZJInc<T>> _filter;

        [Preserve]
        public EcsOneFrameSystem(IEcsFilter<ZJInc<T>> filter)
        {
            _filter = filter ?? throw new ArgumentNullException(nameof(filter));
        }

        public void Run(IEcsSystems systems)
        {
            foreach (var entity in _filter.Value) _filter.Pools.Inc1.Del(entity);
        }

        public class Factory : IOneFrameSystemFactory
        {
            private readonly DiContainer _container;

            public Factory(DiContainer container)
            {
                _container = container ?? throw new ArgumentNullException(nameof(container));
            }

            public IEcsSystem Create()
            {
                return _container.Instantiate<EcsOneFrameSystem<T>>();
            }
        }
    }
}