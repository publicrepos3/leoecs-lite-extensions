using Leopotam.EcsLite;

namespace LeoECSLiteExtensions.OneFrame.Intefaces
{
    public interface IOneFrameSystemFactory
    {
        public IEcsSystem Create();
    }
}