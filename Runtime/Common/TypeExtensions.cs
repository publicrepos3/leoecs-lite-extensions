using System;
using System.Collections.Generic;
using System.Linq;

namespace LeoECSLiteExtensions.Common
{
    internal static class TypeExtensions
    {
        private static readonly Type[] BlackList =
        {
            typeof(void)
        };

        internal static IEnumerable<Type> FindTypesWithAttribute<TAttribute>() where TAttribute : Attribute
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            return assemblies.SelectMany(assembly =>
                assembly.GetTypes()
                    .Where(type =>
                        !type.IsAbstract
                        && !type.IsInterface
                        && (type.IsClass || type.IsValueType)
                        && type.GetCustomAttributes(typeof(TAttribute), false).Any())
            );
        }

        internal static List<Type> GetAllPossibleClosedTypes(this Type openGenericType)
        {
            if (openGenericType == null) throw new ArgumentNullException(nameof(openGenericType));

            if (!openGenericType.IsGenericTypeDefinition)
                throw new ArgumentException($"type {openGenericType} should be GenericTypeDefinition");

            var closedTypes = new List<Type>();

            var genericArguments = openGenericType.GetGenericArguments();
            var suitableParametersPerGeneric = genericArguments
                .Select(genericArgument => GetSuitableGenericParameters(genericArgument).Except(BlackList));

            var cartesianProduct = suitableParametersPerGeneric.CartesianProduct();

            foreach (var parameterCombination in cartesianProduct)
            {
                var closedType = openGenericType.MakeGenericType(parameterCombination.ToArray());
                closedTypes.Add(closedType);
            }

            return closedTypes;
        }

        private static IEnumerable<Type> GetSuitableGenericParameters(Type constraintType)
        {
            var constraints = constraintType.GetGenericParameterConstraints();

            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => !type.IsAbstract
                               && !type.IsInterface
                               && (
                                   constraints.Length == 0
                                   || constraints.All(constraint => constraint.IsAssignableFrom(type))
                               )
                );
        }
    }
}