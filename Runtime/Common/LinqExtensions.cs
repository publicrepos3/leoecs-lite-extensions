﻿using System.Collections.Generic;
using System.Linq;

namespace LeoECSLiteExtensions.Common
{
    internal static class LinqExtensions
    {
        /// <summary>
        ///     string[] colors = { "Red", "Green", "Blue" };
        ///     string[] sizes = { "Small", "Medium", "Large" };
        ///     List
        ///     <string[]>
        ///         data = new List
        ///         <string[]>
        ///             ();
        ///             data.Add(colors);
        ///             data.Add(sizes);
        ///             var result = CartesianProduct(data);
        ///             the result will be:
        ///             Red,    Small
        ///             Red,    Medium
        ///             Red,    Large
        ///             Green,  Small
        ///             Green,  Medium
        ///             Green,  Large
        ///             Blue,   Small
        ///             Blue,   Medium
        ///             Blue,   Large
        /// </summary>
        /// <param name="sequences"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> result = new[] {Enumerable.Empty<T>()};
            foreach (var sequence in sequences)
            {
                var s = sequence;
                result =
                    from seq in result
                    from item in s
                    select seq.Concat(new[] {item});
            }

            return result;
        }

        internal static IEnumerable<T> FromParams<T>(params T[] source)
        {
            return source.AsEnumerable();
        }
    }
}