using System.Collections.Generic;
using System.Threading;
using LeoECSLiteExtensions.ComponentState.Components;
using Leopotam.EcsLite;
using UnityEngine.Scripting;

namespace LeoECSLiteExtensions.ComponentState.Systems
{
    /// <summary>
    ///     reacts on <see cref="Started{T}" /> and <see cref="Finished{T}" /><br />
    ///     creates <see cref="State{T}" /> with <see cref="State{T}.Token" /> which will be cancelled when <see cref="T" />
    ///     will be removed or entity destroyed<br />
    ///     requires <see cref="EcsComponentObserverSystem{T}" /> run first
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EcsComponentStateSystem<T> : IEcsRunSystem, IEcsPreInitSystem
        where T : struct
    {
        private readonly Dictionary<EcsPackedEntityWithWorld, CancellationTokenSource> _activeEntities
            = new(new EcsPackedEntityWithWorldComparer());

        private readonly List<EcsPackedEntityWithWorld> _tempBuffer = new();
        private EcsWorld _ecsWorld;
        private EcsFilter _finishedFilter;

        private EcsFilter _startedFilter;

        private EcsPool<State<T>> _state;
        private EcsPool<StateOfficial<T>> _stateOfficial;

        [Preserve]
        public EcsComponentStateSystem()
        {
        }

        public void PreInit(IEcsSystems systems)
        {
            _ecsWorld = systems.GetWorld();

            _startedFilter = _ecsWorld.Filter<Started<T>>().End();
            _finishedFilter = _ecsWorld.Filter<Finished<T>>().End();

            _state = _ecsWorld.GetPool<State<T>>();
            _stateOfficial = _ecsWorld.GetPool<StateOfficial<T>>();
        }

        public void Run(IEcsSystems systems)
        {
            _tempBuffer.Clear();

            foreach (var key in _activeEntities.Keys) _tempBuffer.Add(key);

            foreach (var key in _tempBuffer)
            {
                if (key.Unpack(out var world, out var entity)) continue;

                var cancellation = _activeEntities[key];
                cancellation.Cancel();
                cancellation.Dispose();
                _activeEntities.Remove(key);
            }

            foreach (var started in _startedFilter)
            {
                ref var stateOfficial = ref _stateOfficial.AddOrGet(started);
                ref var state = ref _state.AddOrGet(started);

                stateOfficial.TokenSource?.Cancel();
                stateOfficial.TokenSource?.Dispose();
                stateOfficial.TokenSource = new CancellationTokenSource();

                state.Token = stateOfficial.TokenSource.Token;

                var packed = _ecsWorld.PackEntityWithWorld(started);
                _activeEntities[packed] = stateOfficial.TokenSource;
            }

            foreach (var finished in _finishedFilter)
            {
                ref var stateOfficial = ref _stateOfficial.AddOrGet(finished);

                stateOfficial.TokenSource?.Cancel();
                stateOfficial.TokenSource?.Dispose();
                stateOfficial.TokenSource = null;

                var packed = _ecsWorld.PackEntityWithWorld(finished);
                _activeEntities.Remove(packed);
            }
        }
    }
}