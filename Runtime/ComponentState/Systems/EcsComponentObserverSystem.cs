using System;
using System.Collections.Generic;
using LeoECSLiteExtensions.ComponentState.Components;
using LeoECSLiteExtensions.ZenjectBridge;
using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using Leopotam.EcsLite;
using UnityEngine.Scripting;

namespace LeoECSLiteExtensions.ComponentState.Systems
{
    /// <summary>
    ///     <inheritdoc cref="EcsComponentObserverSystemBase{T}" /><br />
    ///     Filtration is based on include <see cref="T" />
    /// </summary>
    /// <typeparam name="T">
    ///     <inheritdoc cref="EcsComponentObserverSystemBase{T}" path="/param[@name='T']" />
    /// </typeparam>
    public class EcsComponentObserverSystem<T> : EcsComponentObserverSystemBase<T>
        where T : struct
    {
        protected override EcsFilter Filter { get; }

        [Preserve]
        public EcsComponentObserverSystem(IEcsFilter<ZJInc<T>> filter)
        {
            Filter = (filter ?? throw new ArgumentNullException(nameof(filter))).Value;
        }
    }

    /// <summary>
    ///     <inheritdoc cref="EcsComponentObserverSystemBase{T}" />
    /// </summary>
    /// <typeparam name="T">
    ///     <inheritdoc cref="EcsComponentObserverSystemBase{T}" path="/param[@name='T']" />
    /// </typeparam>
    /// <typeparam name="TInc">include filter</typeparam>
    public class EcsComponentObserverSystem<T, TInc> : EcsComponentObserverSystemBase<T>
        where T : struct
        where TInc : struct, IEcsFilterInc
    {
        protected override EcsFilter Filter { get; }
        
        [Preserve]
        public EcsComponentObserverSystem(IEcsFilter<TInc> filter)
        {
            Filter = (filter ?? throw new ArgumentNullException(nameof(filter))).Value;
        }
    }

    /// <summary>
    ///     <inheritdoc cref="EcsComponentObserverSystemBase{T}" />
    /// </summary>
    /// <typeparam name="T">
    ///     <inheritdoc cref="EcsComponentObserverSystemBase{T}" path="/param[@name='T']" />
    /// </typeparam>
    /// <typeparam name="TInc">include filter</typeparam>
    /// <typeparam name="TExc">exclude filter</typeparam>
    public class EcsComponentObserverSystem<T, TInc, TExc> : EcsComponentObserverSystemBase<T>
        where T : struct
        where TInc : struct, IEcsFilterInc
        where TExc : struct, IEcsFilterExc
    {
        protected override EcsFilter Filter { get; }

        [Preserve]
        public EcsComponentObserverSystem(IEcsFilter<TInc, TExc> filter)
        {
            Filter = (filter ?? throw new ArgumentNullException(nameof(filter))).Value;
        }
    }


    /// <summary>
    ///     based on <see cref="EcsComponentObserverSystemBase{T}.Filter" /> filter entities.<br />
    ///     when entity appears in <see cref="EcsComponentObserverSystemBase{T}.Filter" />, rise <see cref="Started{T}" /> for
    ///     one iteration.<br />
    ///     when entity disappears in <see cref="EcsComponentObserverSystemBase{T}.Filter" />, rise <see cref="Finished{T}" />
    ///     for one iteration<br />
    ///     when entity removes does nothing.
    /// </summary>
    /// <typeparam name="T">base struct to rise <see cref="Started{T}" /> and <see cref="Finished{T}" /></typeparam>
    public abstract class EcsComponentObserverSystemBase<T> : IEcsRunSystem, IEcsPreInitSystem
        where T : struct
    {
        private readonly HashSet<int> _currentFrameEntity = new();

        private readonly HashSet<int> _lastFrameEntity = new();

        private readonly HashSet<EcsPackedEntityWithWorld> _lastFrameEntityKeeper
            = new(new EcsPackedEntityWithWorldComparer());

        private readonly HashSet<int> _tempBuffer = new();
        private EcsWorld _ecsWorld;
        private EcsFilter _finishedFilter;
        private EcsPool<Finished<T>> _finishedPool;

        private EcsFilter _startedFilter;

        private EcsPool<Started<T>> _startedPool;

        protected abstract EcsFilter Filter { get; }

        [Preserve]
        public EcsComponentObserverSystemBase()
        {
        }

        public virtual void PreInit(IEcsSystems systems)
        {
            _ecsWorld = systems.GetWorld();

            _startedFilter = _ecsWorld.Filter<Started<T>>().End();
            _finishedFilter = _ecsWorld.Filter<Finished<T>>().End();

            _startedPool = _ecsWorld.GetPool<Started<T>>();
            _finishedPool = _ecsWorld.GetPool<Finished<T>>();
        }

        public void Run(IEcsSystems systems)
        {
            _lastFrameEntity.Clear();
            _currentFrameEntity.Clear();

            foreach (var entity in _startedFilter) _startedPool.Del(entity);

            foreach (var entity in _finishedFilter) _finishedPool.Del(entity);

            foreach (var packed in _lastFrameEntityKeeper)
            {
                if (!packed.Unpack(out _, out var entity)) continue;

                _lastFrameEntity.Add(entity);
            }

            _lastFrameEntityKeeper.Clear();

            foreach (var entity in Filter)
            {
                _currentFrameEntity.Add(entity);
                _lastFrameEntityKeeper.Add(_ecsWorld.PackEntityWithWorld(entity));
            }

            #region FinishEvent

            _tempBuffer.Clear();
            foreach (var entity in _lastFrameEntity) _tempBuffer.Add(entity);

            _tempBuffer.ExceptWith(_currentFrameEntity);

            foreach (var entity in _tempBuffer) _finishedPool.Add(entity);

            #endregion

            #region StartEvent

            _tempBuffer.Clear();
            foreach (var entity in _currentFrameEntity) _tempBuffer.Add(entity);

            _tempBuffer.ExceptWith(_lastFrameEntity);

            foreach (var entity in _tempBuffer) _startedPool.Add(entity);

            #endregion
        }
    }
}