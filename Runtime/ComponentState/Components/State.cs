using System.Threading;

namespace LeoECSLiteExtensions.ComponentState.Components
{
    public struct State<T> where T : struct
    {
        public CancellationToken Token;
    }
}