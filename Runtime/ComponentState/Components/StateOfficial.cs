#nullable enable
using System.Threading;

namespace LeoECSLiteExtensions.ComponentState.Components
{
    public struct StateOfficial<T> where T : struct
    {
        public CancellationTokenSource? TokenSource;
    }
}