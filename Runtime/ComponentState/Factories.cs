﻿using System;
using LeoECSLiteExtensions.ComponentState.Systems;
using LeoECSLiteExtensions.ZenjectBridge;
using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions.ComponentState
{
    public class EcsComponentObserverSystemFactory<T> : IFactory<T, EcsComponentObserverSystemBase<T>>
        where T : struct
    {
        private readonly IEcsFilter<ZJInc<T>> _filter;

        [Preserve]
        public EcsComponentObserverSystemFactory(IEcsFilter<ZJInc<T>> filter)
        {
            _filter = filter ?? throw new ArgumentNullException(nameof(filter));
        }

        public EcsComponentObserverSystemBase<T> Create(T param)
        {
            return new EcsComponentObserverSystem<T>(_filter);
        }
    }
        
    public class EcsComponentObserverSystemFactory<T, TInc> : IFactory<T, EcsComponentObserverSystemBase<T>>
        where T : struct
        where TInc : struct, IEcsFilterInc
    {
        private readonly IEcsFilter<TInc> _filter;

        [Preserve]
        public EcsComponentObserverSystemFactory(IEcsFilter<TInc> filter)
        {
            _filter = filter ?? throw new ArgumentNullException(nameof(filter));
        }

        public EcsComponentObserverSystemBase<T> Create(T param)
        {
            return new EcsComponentObserverSystem<T, TInc>(_filter);
        }
    }
        
    public class EcsComponentObserverSystemFactory<T, TInc, TExc> : IFactory<T, EcsComponentObserverSystemBase<T>>
        where T : struct
        where TInc : struct, IEcsFilterInc
        where TExc : struct, IEcsFilterExc
    {
        private readonly IEcsFilter<TInc, TExc> _filter;

        [Preserve]
        public EcsComponentObserverSystemFactory(IEcsFilter<TInc, TExc> filter)
        {
            _filter = filter ?? throw new ArgumentNullException(nameof(filter));
        }

        public EcsComponentObserverSystemBase<T> Create(T param)
        {
            return new EcsComponentObserverSystem<T, TInc, TExc>(_filter);
        }
    }
}