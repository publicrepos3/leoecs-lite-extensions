﻿using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions.ComponentState
{
    public class ComponentStateInstaller : Installer<ComponentStateInstaller>
    {
        [Preserve]
        public ComponentStateInstaller()
        {
        }

        public override void InstallBindings()
        {
            Container
                .Bind(typeof(EcsComponentObserverSystemFactory<>))
                .AsSingle();

            Container
                .Bind(typeof(EcsComponentObserverSystemFactory<,>))
                .AsSingle();

            Container
                .Bind(typeof(EcsComponentObserverSystemFactory<,,>))
                .AsSingle();
        }
    }
}