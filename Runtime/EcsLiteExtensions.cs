﻿using System;
using System.Reactive.Disposables;
using System.Threading;
using Cysharp.Threading.Tasks;
using Leopotam.EcsLite;
using UniRx;
using Random = UnityEngine.Random;

namespace LeoECSLiteExtensions
{
    public static class EcsLiteExtensions
    {
        public static CancellationTokenRegistration AddTo(this IEcsSystems ecsSystems, CancellationToken token)
        {
            var disposableWrapper = new EcsSystemsDisposableWrapper(ecsSystems);
            IDisposable disposable = disposableWrapper;

            return disposable
                .AddTo(token);
        }

        public static IEcsSystems AddTo(this IEcsSystems ecsSystems, CompositeDisposable compositeDisposable)
        {
            var disposableWrapper = new EcsSystemsDisposableWrapper(ecsSystems);
            IDisposable disposable = disposableWrapper;

            disposable
                .AddTo(compositeDisposable);

            return disposableWrapper;
        }

        public static IEcsSystems Deferred(
            this IEcsSystems ecsSystems,
            TimeSpan timeout,
            CancellationToken token = default,
            bool runImmediately = true,
            bool randomizeFirstRun = true,
            PlayerLoopTiming delayTiming = PlayerLoopTiming.Update)
        {
            ecsSystems.Init();

            Timer(timeout, ecsSystems, token, runImmediately, randomizeFirstRun, delayTiming).Forget();

            return ecsSystems;
        }

        public static ref T AddOrGet<T>(this EcsPool<T> pool, int entity) where T : struct
        {
            if (pool.Has(entity)) return ref pool.Get(entity);

            return ref pool.Add(entity);
        }

        private static async UniTaskVoid Timer(
            TimeSpan timeout,
            IEcsSystems ecsSystems,
            CancellationToken token = default,
            bool runImmediately = true,
            bool randomizeFirstRun = true,
            PlayerLoopTiming delayTiming = PlayerLoopTiming.Update)
        {
            try
            {
                if (runImmediately)
                    ecsSystems.Run();

                var firstRunTimeout = randomizeFirstRun
                    ? TimeSpan.FromMilliseconds(Random.Range(0f, (float) timeout.TotalMilliseconds))
                    : timeout;

                await UniTask.Delay(firstRunTimeout, cancellationToken: token, ignoreTimeScale: true,
                    delayTiming: delayTiming);

                ecsSystems.Run();

                while (!token.IsCancellationRequested)
                {
                    await UniTask.Delay(timeout, cancellationToken: token, ignoreTimeScale: true,
                        delayTiming: delayTiming);

                    ecsSystems.Run();
                }
            }
            catch (OperationCanceledException canceledException)
            {
            }
        }
    }
}