namespace LeoECSLiteExtensions.Groups.Components
{
    public struct EcsGroupSystemState<T>
    {
        public T Name;
        public bool State;
    }
}