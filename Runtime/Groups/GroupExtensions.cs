#nullable enable
using System.Collections.Generic;
using LeoECSLiteExtensions.Groups.Systems;
using Leopotam.EcsLite;

namespace LeoECSLiteExtensions.Groups
{
    public static class GroupExtensions
    {
        public static IEcsSystems AddGroup<T>(
            this IEcsSystems systems, 
            T groupName, 
            bool defaultState,
            string eventWorldName, 
            IEqualityComparer<T>? comparer = null,
            params IEcsSystem[] nestedSystems)
        {
            var realComparer = comparer ?? EqualityComparer<T>.Default;
            var groupSystem =
                new EcsGroupSystem<T>(groupName, defaultState, eventWorldName, realComparer, nestedSystems);
            
            return systems.Add(groupSystem);
        }
    }
}