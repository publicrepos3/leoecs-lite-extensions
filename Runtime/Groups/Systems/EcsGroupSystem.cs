using System;
using System.Collections.Generic;
using LeoECSLiteExtensions.Groups.Components;
using Leopotam.EcsLite;
using UnityEngine.Scripting;

namespace LeoECSLiteExtensions.Groups.Systems
{
    public class EcsGroupSystem<T> :
        IEcsPreInitSystem,
        IEcsInitSystem,
        IEcsRunSystem,
        IEcsPostRunSystem,
        IEcsDestroySystem,
        IEcsPostDestroySystem
    {
        readonly IEcsSystem[] _allSystems;
        readonly IEcsRunSystem[] _runSystems;
        readonly int _runSystemsCount;
        readonly IEcsPostRunSystem[] _postRunSystems;
        readonly int _postRunSystemsCount;
        readonly string _eventsWorldName;
        private readonly IEqualityComparer<T> _comparer;
        readonly T _name;
        EcsFilter _filter;
        EcsPool<EcsGroupSystemState<T>> _pool;
        bool _state;

        public IEcsSystem[] GetNestedSystems()
        {
            return _allSystems;
        }

        [Preserve]
        public EcsGroupSystem(
            T name,
            bool defaultState,
            string eventsWorldName,
            IEqualityComparer<T> comparer,
            params IEcsSystem[] systems)
        {
            _name = name ?? throw new ArgumentNullException(nameof(name));
            _state = defaultState;
            _eventsWorldName = eventsWorldName;
            _comparer = comparer ?? throw new ArgumentNullException(nameof(comparer));
            _allSystems = systems;
            _runSystemsCount = 0;
            _runSystems = new IEcsRunSystem[_allSystems.Length];
            _postRunSystems = new IEcsPostRunSystem[_allSystems.Length];
            for (var i = 0; i < _allSystems.Length; i++)
            {
                if (_allSystems[i] is IEcsRunSystem runSystem)
                {
                    _runSystems[_runSystemsCount++] = runSystem;
                }

                if (_allSystems[i] is IEcsPostRunSystem postRunSystem)
                {
                    _postRunSystems[_postRunSystemsCount++] = postRunSystem;
                }
            }
        }

        public void PreInit(IEcsSystems systems)
        {
            var world = systems.GetWorld(_eventsWorldName);
            _pool = world.GetPool<EcsGroupSystemState<T>>();
            _filter = world.Filter<EcsGroupSystemState<T>>().End();

            for (var i = 0; i < _allSystems.Length; i++)
            {
                if (_allSystems[i] is IEcsPreInitSystem preInitSystem)
                {
                    preInitSystem.PreInit(systems);
                }
            }
        }

        public void Init(IEcsSystems systems)
        {
            for (var i = 0; i < _allSystems.Length; i++)
            {
                if (_allSystems[i] is IEcsInitSystem initSystem)
                {
                    initSystem.Init(systems);
                }
            }
        }

        public void Run(IEcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var evt = ref _pool.Get(entity);
                if (_comparer.Equals(evt.Name, _name))
                {
                    _state = evt.State;
                    _pool.Del(entity);
                }
            }

            if (_state)
            {
                for (var i = 0; i < _runSystemsCount; i++)
                {
                    _runSystems[i].Run(systems);
                }
            }
        }

        public void PostRun(IEcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var evt = ref _pool.Get(entity);
                if (_comparer.Equals(evt.Name, _name))
                {
                    _state = evt.State;
                    _pool.Del(entity);
                }
            }

            if (_state)
            {
                for (var i = 0; i < _postRunSystemsCount; i++)
                {
                    _postRunSystems[i].PostRun(systems);
                }
            }
        }

        public void Destroy(IEcsSystems systems)
        {
            for (var i = _allSystems.Length - 1; i >= 0; i--)
            {
                if (_allSystems[i] is IEcsDestroySystem destroySystem)
                {
                    destroySystem.Destroy(systems);
                }
            }
        }

        public void PostDestroy(IEcsSystems systems)
        {
            for (var i = _allSystems.Length - 1; i >= 0; i--)
            {
                if (_allSystems[i] is IEcsPostDestroySystem postDestroySystem)
                {
                    postDestroySystem.PostDestroy(systems);
                }
            }
        }
    }
}