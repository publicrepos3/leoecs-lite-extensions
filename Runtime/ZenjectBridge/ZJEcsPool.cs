using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using Leopotam.EcsLite;
using UnityEngine.Scripting;

namespace LeoECSLiteExtensions.ZenjectBridge
{
    public class ZJEcsPool<T> : IEcsPool<T>
        where T : struct
    {
        [Preserve]
        public ZJEcsPool(EcsWorld world)
        {
            Value = world.GetPool<T>();
        }

        public EcsPool<T> Value { get; }
    }
}