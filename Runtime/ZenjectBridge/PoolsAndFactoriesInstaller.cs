using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions.ZenjectBridge
{
    public class PoolsAndFactoriesInstaller : Installer<PoolsAndFactoriesInstaller>
    {
        [Preserve]
        public PoolsAndFactoriesInstaller()
        {
        }

        public override void InstallBindings()
        {
            Container
                .Bind(typeof(ZJInc<>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,,,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,,,,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,,,,,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJInc<,,,,,,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJExc<>))
                .AsSingle();

            Container
                .Bind(typeof(ZJExc<,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJExc<,,>))
                .AsSingle();

            Container
                .Bind(typeof(ZJExc<,,,>))
                .AsSingle();

            Container
                .Bind(typeof(IEcsFilter<>))
                .To(typeof(EcsFilter<>))
                .AsSingle();

            Container
                .Bind(typeof(IEcsFilter<,>))
                .To(typeof(EcsFilter<,>))
                .AsSingle();

            Container
                .Bind(typeof(IEcsPool<>))
                .To(typeof(ZJEcsPool<>))
                .AsSingle();
        }
    }
}