using Leopotam.EcsLite;

namespace LeoECSLiteExtensions.ZenjectBridge.Interfaces
{
    public interface IEcsPool<T>
        where T : struct
    {
        EcsPool<T> Value { get; }
    }
}