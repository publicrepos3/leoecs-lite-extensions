using System.Collections;
using Leopotam.EcsLite;

namespace LeoECSLiteExtensions.ZenjectBridge.Interfaces
{
    public interface IEcsFilterInc
    {
        EcsWorld.Mask Fill(EcsWorld world);
    }

    public interface IEcsFilterExc
    {
        EcsWorld.Mask Fill(EcsWorld.Mask mask);
    }

    public interface IEcsFilter
    {
        EcsWorld World { get; }
        EcsFilter Value { get; }
        EcsFilter.Enumerator GetEnumerator();
    }

    public interface IEcsFilter<out TInc> : IEcsFilter
        where TInc : struct, IEcsFilterInc
    {
        TInc Pools { get; }
    }

    public interface IEcsFilter<out TInc, out TExc> : IEcsFilter
        where TInc : struct, IEcsFilterInc
        where TExc : struct, IEcsFilterExc
    {
        TInc Pools { get; }
        TExc ExcPools { get; }
    }
}