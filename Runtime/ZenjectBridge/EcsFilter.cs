using System;
using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using Leopotam.EcsLite;
using UnityEngine.Scripting;

namespace LeoECSLiteExtensions.ZenjectBridge
{
    public class EcsFilter<TInc> : IEcsFilter<TInc>
        where TInc : struct, IEcsFilterInc
    {
        [Preserve]
        public EcsFilter(EcsWorld world, TInc filterInc)
        {
            World = world ?? throw new ArgumentNullException(nameof(world));
            Pools = filterInc;
            Value = filterInc.Fill(world).End();
        }

        public EcsWorld World { get; }
        public EcsFilter Value { get; }
        public TInc Pools { get; }

        public EcsFilter.Enumerator GetEnumerator()
        {
            return Value.GetEnumerator();
        }
    }

    public class EcsFilter<TInc, TExc> : IEcsFilter<TInc, TExc>
        where TInc : struct, IEcsFilterInc
        where TExc : struct, IEcsFilterExc
    {
        [Preserve]
        public EcsFilter(EcsWorld world, TInc filterInc, TExc filterExc)
        {
            World = world ?? throw new ArgumentNullException(nameof(world));
            Pools = filterInc;
            ExcPools = filterExc;
            Value = filterExc.Fill(filterInc.Fill(world)).End();
        }

        public EcsWorld World { get; }
        public EcsFilter Value { get; }
        public TInc Pools { get; }
        public TExc ExcPools { get; }

        public EcsFilter.Enumerator GetEnumerator()
        {
            return Value.GetEnumerator();
        }
    }

    #region Inc

    public readonly struct ZJInc<T1> : IEcsFilterInc
        where T1 : struct
    {
        public readonly EcsPool<T1> Inc1;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>();
        }
    }

    public readonly struct ZJInc<T1, T2> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>();
        }
    }

    public readonly struct ZJInc<T1, T2, T3> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
        where T3 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;
        public readonly EcsPool<T3> Inc3;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
            Inc3 = world.GetPool<T3>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>().Inc<T3>();
        }
    }

    public readonly struct ZJInc<T1, T2, T3, T4> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
        where T3 : struct
        where T4 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;
        public readonly EcsPool<T3> Inc3;
        public readonly EcsPool<T4> Inc4;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
            Inc3 = world.GetPool<T3>();
            Inc4 = world.GetPool<T4>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>().Inc<T3>().Inc<T4>();
        }
    }

    public readonly struct ZJInc<T1, T2, T3, T4, T5> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
        where T3 : struct
        where T4 : struct
        where T5 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;
        public readonly EcsPool<T3> Inc3;
        public readonly EcsPool<T4> Inc4;
        public readonly EcsPool<T5> Inc5;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
            Inc3 = world.GetPool<T3>();
            Inc4 = world.GetPool<T4>();
            Inc5 = world.GetPool<T5>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>().Inc<T3>().Inc<T4>().Inc<T5>();
        }
    }

    public readonly struct ZJInc<T1, T2, T3, T4, T5, T6> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
        where T3 : struct
        where T4 : struct
        where T5 : struct
        where T6 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;
        public readonly EcsPool<T3> Inc3;
        public readonly EcsPool<T4> Inc4;
        public readonly EcsPool<T5> Inc5;
        public readonly EcsPool<T6> Inc6;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
            Inc3 = world.GetPool<T3>();
            Inc4 = world.GetPool<T4>();
            Inc5 = world.GetPool<T5>();
            Inc6 = world.GetPool<T6>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>().Inc<T3>().Inc<T4>().Inc<T5>().Inc<T6>();
        }
    }

    public readonly struct ZJInc<T1, T2, T3, T4, T5, T6, T7> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
        where T3 : struct
        where T4 : struct
        where T5 : struct
        where T6 : struct
        where T7 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;
        public readonly EcsPool<T3> Inc3;
        public readonly EcsPool<T4> Inc4;
        public readonly EcsPool<T5> Inc5;
        public readonly EcsPool<T6> Inc6;
        public readonly EcsPool<T7> Inc7;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
            Inc3 = world.GetPool<T3>();
            Inc4 = world.GetPool<T4>();
            Inc5 = world.GetPool<T5>();
            Inc6 = world.GetPool<T6>();
            Inc7 = world.GetPool<T7>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>().Inc<T3>().Inc<T4>().Inc<T5>().Inc<T6>().Inc<T7>();
        }
    }

    public readonly struct ZJInc<T1, T2, T3, T4, T5, T6, T7, T8> : IEcsFilterInc
        where T1 : struct
        where T2 : struct
        where T3 : struct
        where T4 : struct
        where T5 : struct
        where T6 : struct
        where T7 : struct
        where T8 : struct
    {
        public readonly EcsPool<T1> Inc1;
        public readonly EcsPool<T2> Inc2;
        public readonly EcsPool<T3> Inc3;
        public readonly EcsPool<T4> Inc4;
        public readonly EcsPool<T5> Inc5;
        public readonly EcsPool<T6> Inc6;
        public readonly EcsPool<T7> Inc7;
        public readonly EcsPool<T8> Inc8;

        [Preserve]
        public ZJInc(EcsWorld world)
        {
            Inc1 = world.GetPool<T1>();
            Inc2 = world.GetPool<T2>();
            Inc3 = world.GetPool<T3>();
            Inc4 = world.GetPool<T4>();
            Inc5 = world.GetPool<T5>();
            Inc6 = world.GetPool<T6>();
            Inc7 = world.GetPool<T7>();
            Inc8 = world.GetPool<T8>();
        }

        EcsWorld.Mask IEcsFilterInc.Fill(EcsWorld world)
        {
            return world.Filter<T1>().Inc<T2>().Inc<T3>().Inc<T4>().Inc<T5>().Inc<T6>().Inc<T7>().Inc<T8>();
        }
    }

    #endregion

    #region Exc

    public readonly struct ZJExc<T1> : IEcsFilterExc
        where T1 : struct
    {
        public readonly EcsPool<T1> Exc1;

        [Preserve]
        public ZJExc(EcsWorld world)
        {
            Exc1 = world.GetPool<T1>();
        }

        EcsWorld.Mask IEcsFilterExc.Fill(EcsWorld.Mask mask)
        {
            return mask.Exc<T1>();
        }
    }

    public readonly struct ZJExc<T1, T2> : IEcsFilterExc
        where T1 : struct
        where T2 : struct
    {
        public readonly EcsPool<T1> Exc1;
        public readonly EcsPool<T2> Exc2;

        [Preserve]
        public ZJExc(EcsWorld world)
        {
            Exc1 = world.GetPool<T1>();
            Exc2 = world.GetPool<T2>();
        }

        EcsWorld.Mask IEcsFilterExc.Fill(EcsWorld.Mask mask)
        {
            return mask.Exc<T1>().Exc<T2>();
        }
    }

    public readonly struct ZJExc<T1, T2, T3> : IEcsFilterExc
        where T1 : struct
        where T2 : struct
        where T3 : struct
    {
        public readonly EcsPool<T1> Exc1;
        public readonly EcsPool<T2> Exc2;
        public readonly EcsPool<T3> Exc3;

        [Preserve]
        public ZJExc(EcsWorld world)
        {
            Exc1 = world.GetPool<T1>();
            Exc2 = world.GetPool<T2>();
            Exc3 = world.GetPool<T3>();
        }

        EcsWorld.Mask IEcsFilterExc.Fill(EcsWorld.Mask mask)
        {
            return mask.Exc<T1>().Exc<T2>().Exc<T3>();
        }
    }

    public readonly struct ZJExc<T1, T2, T3, T4> : IEcsFilterExc
        where T1 : struct
        where T2 : struct
        where T3 : struct
        where T4 : struct
    {
        public readonly EcsPool<T1> Exc1;
        public readonly EcsPool<T2> Exc2;
        public readonly EcsPool<T3> Exc3;
        public readonly EcsPool<T4> Exc4;

        [Preserve]
        public ZJExc(EcsWorld world)
        {
            Exc1 = world.GetPool<T1>();
            Exc2 = world.GetPool<T2>();
            Exc3 = world.GetPool<T3>();
            Exc4 = world.GetPool<T4>();
        }

        EcsWorld.Mask IEcsFilterExc.Fill(EcsWorld.Mask mask)
        {
            return mask.Exc<T1>().Exc<T2>().Exc<T3>().Exc<T4>();
        }
    }

    #endregion
}