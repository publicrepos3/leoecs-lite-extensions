﻿using LeoECSLiteExtensions.ComponentFactories;
using LeoECSLiteExtensions.ComponentState;
using LeoECSLiteExtensions.OneFrame;
using LeoECSLiteExtensions.ZenjectBridge;
using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions
{
    public class LeoECSLiteExtensionsInstaller : Installer<LeoECSLiteExtensionsInstaller>
    {
        [Preserve]
        public LeoECSLiteExtensionsInstaller()
        {
        }

        public override void InstallBindings()
        {
            PoolsAndFactoriesInstaller.Install(Container);
            ComponentStateInstaller.Install(Container);
            ComponentFactoriesInstaller.Install(Container);
            EcsOneFrameInstaller.Install(Container);
        }
    }
}