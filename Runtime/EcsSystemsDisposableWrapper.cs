using System;
using System.Collections.Generic;
using Leopotam.EcsLite;

namespace LeoECSLiteExtensions
{
    internal class EcsSystemsDisposableWrapper : IEcsSystems, IDisposable
    {
        private readonly IEcsSystems _ecsSystems;

        public EcsSystemsDisposableWrapper(IEcsSystems ecsSystems)
        {
            _ecsSystems = ecsSystems ?? throw new ArgumentNullException(nameof(ecsSystems));
        }

        public void Dispose()
        {
            _ecsSystems.Destroy();
        }

        public T GetShared<T>() where T : class
        {
            return _ecsSystems.GetShared<T>();
        }

        public IEcsSystems AddWorld(EcsWorld world, string name)
        {
            return _ecsSystems.AddWorld(world, name);
        }

        public EcsWorld GetWorld(string name = null)
        {
            return _ecsSystems.GetWorld(name);
        }

        public Dictionary<string, EcsWorld> GetAllNamedWorlds()
        {
            return _ecsSystems.GetAllNamedWorlds();
        }

        public IEcsSystems Add(IEcsSystem system)
        {
            return _ecsSystems.Add(system);
        }

        public List<IEcsSystem> GetAllSystems()
        {
            return _ecsSystems.GetAllSystems();
        }

        public void Init()
        {
            _ecsSystems.Init();
        }

        public void Run()
        {
            _ecsSystems.Run();
        }

        public void Destroy()
        {
            _ecsSystems.Destroy();
        }
    }
}