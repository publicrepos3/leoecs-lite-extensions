using System;
using System.Collections.Generic;
using Leopotam.EcsLite;

namespace LeoECSLiteExtensions
{
    public class EcsPackedEntityWithWorldComparer : IEqualityComparer<EcsPackedEntityWithWorld>
    {
        public bool Equals(EcsPackedEntityWithWorld x, EcsPackedEntityWithWorld y)
        {
            return x.EqualsTo(y);
        }

        public int GetHashCode(EcsPackedEntityWithWorld obj)
        {
            obj.Unpack(out var world, out var entity);

            return HashCode.Combine(world.GetHashCode(), entity);
        }
    }
}