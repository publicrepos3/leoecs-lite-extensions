using System;
using LeoECSLiteExtensions.ComponentFactories.Interfaces;
using LeoECSLiteExtensions.ZenjectBridge.Interfaces;
using UnityEngine.Scripting;

namespace LeoECSLiteExtensions.ComponentFactories
{
    public class ComponentFactory<T> : IComponentFactory<T>
        where T : struct
    {
        private readonly IEcsPool<T> _pool;

        [Preserve]
        public ComponentFactory(IEcsPool<T> pool)
        {
            _pool = pool ?? throw new ArgumentNullException(nameof(pool));
        }

        public ref T Create(int entity)
        {
            ref var component = ref _pool.Value.AddOrGet(entity);

            return ref component;
        }
    }
}