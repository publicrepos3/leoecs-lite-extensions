﻿namespace LeoECSLiteExtensions.ComponentFactories.Interfaces
{
    public interface IComponentFactory<T>
        where T : struct
    {
        ref T Create(int entity);
    }
}