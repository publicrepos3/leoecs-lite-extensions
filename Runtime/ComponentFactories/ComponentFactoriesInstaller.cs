using LeoECSLiteExtensions.ComponentFactories.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace LeoECSLiteExtensions.ComponentFactories
{
    public class ComponentFactoriesInstaller : Installer<ComponentFactoriesInstaller>
    {
        [Preserve]
        public ComponentFactoriesInstaller()
        {
        }

        public override void InstallBindings()
        {
            Container
                .Bind(typeof(IComponentFactory<>))
                .To(typeof(ComponentFactory<>))
                .AsSingle();
        }
    }
}